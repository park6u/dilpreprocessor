"""
    Feed-forward(Linear-Layer)
    using FloatTensor and matmul(Matrix Multiplication)
"""
from torch import FloatTensor, matmul as mm
from torch.nn import Linear

w = FloatTensor([1, 2])
b = FloatTensor([2, 2])
x = FloatTensor([1, 2])


def linear(x, w, b):
    """


        KeyWord arguments
        x --
        w --
        b --
    """
    return mm(x, w) + b


y = linear(x, w, b)
print(x.size())
print(x.size())


linear = Linear(3, 2)
