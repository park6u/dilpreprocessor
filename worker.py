import argparse
from crawler.url_crawler import URkCrawler
from crawler.naver_crawler import NaverCrawler
from crawler.ndsl_crawler import MDSLCrawler


class Worker:
    _args = None

    def __init__(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-u', '-url', dest='url', nargs=6, help='domain to collect document; [args.domain, args.title, args.year, args.month, args.start, args.end]')
        parser.add_argument('-t', '-target', dest='target', required=False, help='target where to collect document, "new" (Naver News) or "ndsl" (NDSL, Thesis DB)')
        parser.add_argument('-k', '-keyword', dest='keyword', required=False, help='keyword to collect document')
        self._args = parser.parse_args()
        self.execute_args()

    def execute_args(self):
        if self._args.url:
            domain, title, year, month, start, end = map(lambda x: x, self._args.url)
            print(domain, title, year, month, start, end)
            crawler = URkCrawler(domain, title, year, month, start, end)
            crawler.work()
        else:
            if self._args.target == 'news':
                NaverCrawler(self._args.keyword)
            elif self._args.target == 'ndsl':
                MDSLCrawler(self._args.keyword)
            else:
                print('Wrong Args')


if __name__ == '__main__':
    Worker()