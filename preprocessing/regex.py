import re
import enum
from bs4 import BeautifulSoup


class Regex:
    __KOREAN_REGEX = '가-힣'
    __ENGLISH_REGEX = 'A-Za-z'
    __NUMBER_REGEX = '0-9'
    __PUNCTUATION_REGEX = '-.,?!\'\"'
    __EMAIL_REGEX = '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)'
    __PH_REGEX = ''
    __URL_REGEX = ''
    __fulltext = ''
    __stopwords_regex = ''

    def __init__(self, fulltext):
        self.__fulltext = fulltext

    def filter_tag(self, html_tag):
        """
        :param html_tag: name of HTML tag
        :return:
        """
        bs = BeautifulSoup(self.__fulltext, 'lxml')
        ret = []            
        self.__fulltext = ''
        for content in bs.find_all(html_tag):
            self.__fulltext += content.get_text()

    def block_text(self, flag: int) -> str:
        pattern = '['
        if (flag & Flag.EMAIL.value)  == Flag.EMAIL.value:
            pattern += self.__EMAIL_REGEX

        pattern += ']+'
        re.split(pattern)

    def leave_text(self, flag: int):
        self.filter_stopwords()
        pattern = '[ '
        if (flag & Flag.KOREAN.value) == Flag.KOREAN.value:
            pattern += self.__KOREAN_REGEX
        if flag & Flag.ENGLISH.value:
            pattern += self.__ENGLISH_REGEX
        if flag & Flag.NUMBER.value:
            pattern += self.__NUMBER_REGEX
        if flag & Flag.PUNCTUATION.value:
            pattern += self.__PUNCTUATION_REGEX

        pattern += ' ]+'

        ret = ''
        for i in re.findall(pattern, self.__fulltext):
            ret += i

        self.__fulltext = ret

    def filter_stopwords(self):
        if self.__stopwords_regex == '':
            return
        self.__fulltext = re.sub(self.__stopwords_regex, ' ', self.__fulltext)

    def set_stopwords(self, words):
        stopwords_regex = ''
        for i in words:
            stopwords_regex += "({0})|".format(i.strip())
        self.__stopwords_regex = stopwords_regex[0:len(stopwords_regex) - 1]

    def filter_pattern(self):
        pass

    def get_fulltext(self):
        return self.__fulltext


class Flag(enum.Enum):
    """
        KOREAN - include korean
        ENGLISH - include english
        NUMBER - include number
        PUNCTUATION - include punctuation
    """
    KOREAN = 0b0001
    ENGLISH = 0b0010
    NUMBER = 0b0100
    PUNCTUATION = 0b1000
    EMAIL = 0b10000
    PH_NUMBER = 0b100000
    URL = 0b1000000


if __name__ == '__main__':
    pass
