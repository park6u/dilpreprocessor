from sys import stdin
from bs4 import BeautifulSoup
from preprocessing.encoding import Encoding


# todo
class BufferRead:
    __BUFFERING_DEFAULT_SIZE = 4096
    __tag_content_buffering_size = 0
    __file = ''
    __file_name = ''
    __content = ''

    """
    :param file: file directory (corpus) to read
    """
    def __init__(self, file: str):
        self.__file_name = file

    """
    parsing file and buffering with HTML Meta Tag
    
    :param encoding: encoding of input file, default is utf-8
    :param binary: read with binary file
    :param metaTag: buffering unit HTML Meta Tag
    
    :var eot: End of HTML Meta Tag </:tag>
    """
    def read(self, encoding: Encoding, binary=True, metaTag='article'):
        if encoding == '' or binary:
            self.__file = open(self.__file_name, 'rb')
        else:
            self.__file = open(self.__file_name, 'r')
        self.__content = self.__file.read()

    def __read_until(self, character: str):
        pass

    def write(self, file):
        pass

    def get(self) -> str:
        return self.__content

    def close(self):
        self.__file.close()


if __name__ == '__main__':
    b = BufferRead('../data/20150901.txt.encoding')
    b.read(Encoding(), False, 'article')
    print(b.get())
    b.close()
