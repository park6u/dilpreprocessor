from chardet import detect


class Encoding:
    encoding = 'utf-8'

    def __init__(self, encoding='utf-8'):
        #detect(file)
        self.encoding = encoding

    def __eq__(self, other):
        if isinstance(other, str):
            return other == self.encoding
        else:
            return other.encoding == self.encoding

    def to_full_width(self):
        pass

if __name__ == '__main__':
    pass
