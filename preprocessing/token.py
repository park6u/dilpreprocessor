from konlpy.tag import _kkma
from konlpy.tag import _hannanum

class Sentences:
    __sentences = []
    __fulltext = ''

    def __init__(self, fulltext):
        self.__fulltext = fulltext

    def split(self):
        kkma = _kkma.Kkma()
        hnn = _hannanum.Hannanum()
        return kkma.sentences(self.__fulltext)


class Tokens:
    __tokens = []
    __sentence = ''

    def __init__(self, sentence):
        self.__sentence = sentence

    def tokenize(self):
        kkma = _kkma.Kkma(max_heap_size=65532)
#        hnn = _hannanum.Hannanum()
        self.__tokens = kkma.nouns(self.__sentence)
        return self.__tokens
