from crawler.crawler import Crawler
from xml.etree.ElementTree import Element, dump
from xml.etree.ElementTree import ElementTree
from random import randrange
from time import time
from time import sleep
import os


class NaverCrawler(Crawler):
    def __init__(self, keyword):
        Crawler.__init__(self, keyword)

    def work(self):
        def indent(elem, level=0):
            i = "\n" + level * "  "
            if len(elem):
                if not elem.text or not elem.text.strip():
                    elem.text = i + "  "
                if not elem.tail or not elem.tail.strip():
                    elem.tail = i
                for elem in elem:
                    indent(elem, level + 1)
                if not elem.tail or not elem.tail.strip():
                    elem.tail = i
            else:
                if level and (not elem.tail or not elem.tail.strip()):
                    elem.tail = i

        files = os.listdir('urls')

        for file_name in files:
            f = open('../data/' + file_name, 'r', encoding='euc-kr')

            urls = f.readlines()
            f.close()
            # meta-data convention eg
            # 1 topic(input word): 헬스케어
            # 2 number of total news: 2432
            # 3 number of news: 957
            # 4 start_date: 2018.01.01
            # 5 end_date: 2018.01.1
            meta_topic = file_name
            # urls[0].split(':')[1].strip()
            meta_start_data = '0' #urls[3].split(':')[1].strip()
            meta_end_date = '1' #urls[4].split(':')[1].strip()
            folder_name = '{0}_{1}~{2}'.format(meta_topic, meta_start_data, meta_end_date)
            os.mkdir('../output/text/' + folder_name)

            # strip meta data
            urls = urls[5:len(urls)-1]

            digit = len(str(len(urls)))
            start = time()
            for index, url in enumerate(urls):
                offset = '0' * (digit - len(str(index)))
                _id = offset + str(index + 1)
                self._browser.get(url)
                root_node = Element('xml')
                title_node = Element('title')
                date_node = Element('date')
                article_node = Element('article')

                if url.find('sports') == -1:
                    # title
                    try:
                        title = self._browser.find_element_by_id('articleTitle')
                        title_node.text = title.text
                        root_node.append(title_node)
                    except Exception:
                        # Entertainment
                        continue

                    # date
                    try:
                        date = self._browser.find_element_by_class_name('t11').text
                        date_node.text = date
                        root_node.append(date_node)
                    except Exception:
                        # Entertainment
                        continue

                    # article
                    try:
                        article_text = self._browser.execute_script("return $('#articleBodyContents')[0].outerText")
                        article_node.text = article_text
                    except Exception:
                        # Entertainment
                        continue

                root_node.append(article_node)
                sleep(randrange(1, 3) * 0.1)
                indent(root_node)
                # write to xml
                ElementTree(root_node).write('../output/text/' + folder_name + '/' + _id + '.xml', encoding='utf-8')
            end = time()
            print(end - start)
