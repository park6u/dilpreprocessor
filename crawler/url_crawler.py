from crawler.crawler import Crawler
import time
import re
import math


class URkCrawler(Crawler):
    _URL_NAVER = 'https://search.naver.com/search.naver?where=news&sm=tab_jum&query='
    _URL_NDSL = ''
    _domain = ''
    _title = ''
    _year = ''
    _month = ''
    _start_date = ''
    _end_date = ''
    _target = ''

    def __init__(self, domain, title, year, month, start_date, end_date, target='news'):
        Crawler.__init__(self)
        self._domain = domain
        self._title = title
        self._year = year
        self._month = month

        self._start_date = start_date
        self._end_date = end_date
        self._target = target

    def work(self):
        if self._target == 'news':
            self._browser.get(self._URL_NAVER)
        elif self._target == 'ndsl':
            self._browser.get(self._URL_NDSL)

        keyword = self._browser.find_element_by_name("query")
        time.sleep(1)
        keyword.clear()

        keyword.send_keys(self._domain)
        time.sleep(0.1)

        keyword.submit()
        time.sleep(0.1)

        self._browser.find_element_by_xpath('//*[@id="_search_option_btn"]').click()
        self._browser.find_element_by_xpath('//*[@id="snb"]/div/ul/li[2]/a').click()
        time.sleep(0.1)
        start_date = self._year + '.' + self._month + '.' + self._start_date
        day_start = self._browser.find_element_by_id("news_input_period_begin")
        day_start.clear()
        day_start.send_keys(start_date)
        time.sleep(0.1)

        end_date = self._year + '.' + self._month + '.' + self._end_date
        day_end = self._browser.find_element_by_id("news_input_period_end")
        day_end.clear()
        day_end.send_keys(end_date)
        time.sleep(0.1)

        self._browser.find_element_by_xpath('//*[@id="_nx_option_date"]/div[2]/span/button/span').click()
        time.sleep(0.1)
        self._browser.find_element_by_xpath('//*[@id="snb"]/div/ul/li[1]/a').click()
        self._browser.find_element_by_xpath('//*[@id="snb"]/div/ul/li[1]/div/ul/li[3]/a').click()
        time.sleep(0.1)
        ##########################################################################################################################
        # 페이지수 구하기
        a = " /"
        # pagenum=self._browser.find_element_by_xpath('//*[@id="main_pack"]/div[2]/div[1]/div[1]/span').text
        pagenum = self._browser.find_element_by_xpath('// *[ @ id = "main_pack"] / div / div[1] / div[1] / span').text

        pagenum = a + pagenum
        pagenublist = pagenum.split("/")
        temp = pagenublist[2]
        temp = re.sub('건', '', temp)
        temp = re.sub(',', '', temp)
        print(temp)  # 총뉴스검색수

        page_num = int(temp)
        temp_page = page_num
        ##########################################################################################################################
        # 여기까지 왔으면 헬스케어, 기간, 오래된순 정렬까지 끝난 상태

        # 여기서 페이지 url을 긁어야 함 > 1페이지 url 한번 수집
        NaverNews_list = []
        NaverNews_list_url = []
        ################################################

        for i in range(1, 11):
            try:
                NaverNews = self._browser.find_element_by_xpath('//*[@id="sp_nws' + str(i) + '"]/dl/dd[1]/a')

                if len(NaverNews.text) != 0:
                    NaverNews_list.append(NaverNews)  # list에 selenium값들을 쭉 append 함
                    NaverNews_list_url.append(NaverNews.get_attribute("href"))
            except:
                pass

        # print(len(NaverNews_list))
        # print(len(NaverNews_list_url))
        # for i in NaverNews_list_url:
        #     print(i)

        #######################################################################################################################
        # 2페이지 진입하고 위의 수집 다시 #2~6페이지까지 url 수집

        lists = [1, 3, 4, 5, 6]
        sp_nws_num = 0
        try:
            for list in lists:
                sp_nws_num = sp_nws_num + 10
                if list < 6:
                    self._browser.find_element_by_xpath('//*[@id="main_pack"]/div[2]/div[2]/a[' + str(list) + ']').click()
                    time.sleep(0.1)
                    for i in range(1 + sp_nws_num,
                                   11 + sp_nws_num):  # 이거 숫자 1~11이 아니라 계속 증가네.............#########################################################
                        try:
                            NaverNews = self._browser.find_element_by_xpath('//*[@id="sp_nws' + str(i) + '"]/dl/dd[1]/a')
                            if len(NaverNews.text) != 0:
                                NaverNews_list.append(NaverNews)
                                NaverNews_list_url.append(NaverNews.get_attribute("href"))
                        except:
                            pass
                elif list > 5:
                    self._browser.find_element_by_xpath('//*[@id="main_pack"]/div[2]/div[2]/a[' + str(6) + ']').click()
                    time.sleep(1)  # 6페이지까지 봄
                    for i in range(1 + sp_nws_num, 11 + sp_nws_num):
                        try:
                            NaverNews = self._browser.find_element_by_xpath('//*[@id="sp_nws' + str(i) + '"]/dl/dd[1]/a')
                            if len(NaverNews.text) != 0:
                                NaverNews_list.append(NaverNews)
                                NaverNews_list_url.append(NaverNews.get_attribute("href"))
                        except:
                            pass
        except:
            pass
        #################################################################################################################
        # a=75-60 #a는 75개라면 총 8페이지까지 가야함

        page_num = page_num - 60
        page_num = page_num / 10
        page_num_total = math.ceil(page_num)
        for i in range(1, page_num_total - 3):
            sp_nws_num = sp_nws_num + 10
            try:
                self._browser.find_element_by_xpath('//*[@id="main_pack"]/div[2]/div[2]/a[' + str(6) + ']').click()
                time.sleep(0.1)
                for i in range(1 + sp_nws_num, 11 + sp_nws_num):
                    try:
                        NaverNews = self._browser.find_element_by_xpath('//*[@id="sp_nws' + str(i) + '"]/dl/dd[1]/a')
                        if len(NaverNews.text) != 0:
                            NaverNews_list.append(NaverNews)
                            NaverNews_list_url.append(NaverNews.get_attribute("href"))
                    except:
                        pass
            except:
                pass
        lists2 = [7, 8, 9, 10]
        for list in lists2:
            sp_nws_num = sp_nws_num + 10
            try:
                if list < 11:
                    self._browser.find_element_by_xpath('//*[@id="main_pack"]/div[2]/div[2]/a[' + str(list) + ']').click()
                    time.sleep(0.1)
                    for i in range(1 + sp_nws_num,
                                   11 + sp_nws_num):  # 이거 숫자 1~11이 아니라 계속 증가네.............#########################################################
                        try:
                            NaverNews = self._browser.find_element_by_xpath('//*[@id="sp_nws' + str(i) + '"]/dl/dd[1]/a')
                            if len(NaverNews.text) != 0:
                                NaverNews_list.append(NaverNews)
                                NaverNews_list_url.append(NaverNews.get_attribute("href"))
                        except:
                            pass
            except:
                pass

        print(len(NaverNews_list_url))  # 네이버뉴스로 된 뉴스 수

        print(NaverNews_list_url[-1])
        ##########################################################################################################
        time.sleep(0.1)

        open_output_file = open('urls/News_' + self._title + '_' + start_date + '~' + end_date + '.txt', 'w')

        open_output_file.write('topic(input word): ' + self._domain + '\n')
        open_output_file.write('number of total news: ' + str(temp_page) + '\n')
        open_output_file.write('number of naver news: ' + str(len(NaverNews_list_url)) + '\n')
        open_output_file.write('start_date: ' + str(start_date) + '\n')
        open_output_file.write('end_date: ' + str(end_date) + '\n')

        for i in NaverNews_list_url:
            open_output_file.write(i + '\n')
        open_output_file.close()
        self._browser.close()
