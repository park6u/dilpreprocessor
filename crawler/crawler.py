from time import sleep
from random import randrange
from selenium import webdriver
from xml.etree.ElementTree import Element, dump
from xml.etree.ElementTree import ElementTree
from selenium.webdriver.chrome.options import Options
from time import time


class Crawler:
    _browser = None

    def __init__(self):
        chrome_option = Options()

        chrome_option.add_experimental_option('prefs',
                                              {
                                                  "plugins.plugins_list": [
                                                      {"enabled": False, "name": "Chrome PDF Viewer"}],
                                                  "download":
                                                      {
                                                          "prompt_for_download": False,
                                                          #                                                          "default_directory": __temp__
                                                      }

                                              }
                                              )

        self._browser = webdriver.Chrome('tools/chromedriver', options=chrome_option)
