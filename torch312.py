import torch
import torch.nn as nn


class RNNClassifier(nn.module):
    def __init__(self, input_size,
                 word_vec_dim, hidden_size, n_classes, n_layer=4, dropout_p=3):
        self.input_size = input_size
        self.word_vec_dim = word_vec_dim
        self.hidden_size = hidden_size
        self.n_classes = n_classes
        self.n_layer = n_layer
        self.dropout_p = dropout_p

        super().__init__()

        self.emb = nn.Embedding(input_size, word_vec_dim)

        """
         input_size: dimension of x
         hidden_size: 
         n_layer: number of layers in RNN
        """
        self.rnn = nn.RNN(input_size=input_size,
                          hidden_size=hidden_size,
                          num_layers=n_layer,
                          batch_first=True,
                          bidirectional=False)

        """
        
        """
        self.generator = nn.Linear(hidden_size * 2, n_classes)

        """
         Activation Function is LogSoftMax
        """
        self.activation = nn.LogSoftmax(dim=-1)

    """
    :param x 
    """
    def forward(self, x):
        # x = (batch_size, length)
        x = self.emb(x)
        # x = (batch_size, length, word_vec_dim)
        x, temp = self.rnn(x)
        #
        y = self.activation(self.generator(x[:, -1]))

        return y

def get_grad_norm(parameters, norm_type=2):
    parameters = list(filter(lambda p: p.grad is not None, parameters))

    total_norm = 0

    try:
        for p in parameters:
            param_norm = p.grad.data.norm(norm_type)
            total_norm += param_norm ** norm_type
        total_norm = total_norm ** (1. / norm_type)
    except Exception as e:
        print(e)

    return total_norm


def get_parameter_norm(parameters, norm_type=2):
    total_norm = 0

    try:
        for p in parameters:
            param_norm = p.data.norm(norm_type)
            total_norm += param_norm ** norm_type
        total_norm = total_norm ** (1. / norm_type)
    except Exception as e:
        print(e)

    return total_norm

class Trainer():
    def __init__(self):
        pass

    @staticmethod
    def step(engine, mini_batch):
        engine.model.train()
        engine.optimizer.zero_grad()

        x, y = mini_batch.text, mini_batch.label

        y_hat = engine.model(x)

        loss = engine.crit(y_hat, y)
        loss.backward()

        p_norm = float(get_grad_norm(engine.model.parameters()))
        g_norm = float(get_parameter_norm(engine.model.parameters()))

        engine.optimizer.step()

        return float(loss), p_norm, g_norm

    @staticmethod
    def validate(engine, mini_batch):

        engine.model.eval()

        with torch.no_grad:
            x, y  = mini_batch, mini_batch.label

            y_hat = engine.model
            loss = engine.crit(y_hat, y)

        return float(loss)

    @staticmethod
    def attach(trainer, evaluator, verbose=2):
        from ignite.engine import Events
        from ignite.metrics import RunningAverage
        from ignite.contrib.handlers.tqdm_logger import ProgressBar

        RunningAverage(output_transform= lambda x: x[0]).attach(trainer, 'loss')
        RunningAverage(output_transform= lambda x: x[0]).attach(trainer, '|param|')
        RunningAverage(output_transform= lambda x: x[0]).attach(trainer, '|g_param|')

        if verbose >= 2:
            pbar = ProgressBar()
            pbar.attach(trainer, ['|param|', '|g_param', 'loss'])

        if verbose >= 1:
            @trainer.on(Events.EPOCH_COMPLETED)
            def print_train_logs(engine):
                avg_p_norm = engine.state.metrics['|param|']
                avg_g_norm = engine.state.metrics['|g_param|']
                avg_loss = engine.state.metrics['loss']

                print('Epoch {} - |param|={:.2e} |g_param|={:.2e} loss={:.4e}'.format(
                    engine.state.epoch,
                    avg_p_norm,
                    avg_g_norm,
                    avg_loss
                ))

        RunningAverage(output_transform=lambda x: x).attach(evaluator, 'loss')

        if verbose >= 2:
            pbar = ProgressBar()
            pbar.attach(evaluator, ['loss'])

        if verbose >= 1:
            @evaluator.on(Events.EPOCH_COMPLETED)
            def print_valid_logs(engine):
                avg_loss = engine.state.metrics['loss']
                print('Validation - loss={:.4e} lowest_loss={:.4e}'.format(avg_loss, engine.lowest_loss))

    def train(self, model, crit, train_loader, valid_loader):
        from torch import optim
        from ignite.engine import Engine, Events
        import numpy as np
        optimizer = optim.Adam(model.parameters())

        trainer = Engine(Trainer.step)
        trainer.model, trainer.crit, trainer.optimizer = model, crit, optimizer

        evaluator = Engine(Trainer.validate)
        evaluator.model, evaluator.crit = model, crit
        evaluator.lowest_loss = np.inf

        Trainer.attach(trainer, evaluator)

        def run_validation(engine, evaluator, valid_loader):
            evaluator.run(valid_loader, max_epochs=1)

        trainer.add_event_handler(
            Events.EPOCH_COMPLETED, run_validation, evaluator, valid_loader
        )

        @evaluator.on(Events.EPOCH_COMPLETED)
        def check_loss(engine):
            from copy import deepcopy

            loss = float(engine.state.metrics['loss'])
            if loss <= engine.lowest_loss:
                engine.lowest_loss = loss
                engine.best_model = deepcopy(engine.model.state_dict())

        trainer.run(train_loader, max_epochs=1)

        return evaluator.best_model


class DataLoader:

    def __init__(self, train_fn, valid_fn,
                 batch_size=64,
                 max_vocab=999999,
                 min_freq=1,
                 use_eos=False,
                 shuffle=True):
        from torchtext import data

        self.text = data.Field(use_vocab=True,
                               batch_first=True,
                               include_lengths=False,
                               eos_token='<EOS>')

        self.label = data.Field(sequential=False,
                                use_vocab=True,
                                unk_token=None)

        train, valid = data.TabularDataset.split(path='',
                                                 train=train_fn,
                                                 validation=valid_fn,
                                                 format='csv',
                                                 fileds=[('label', self.label),
                                                         ('text', self.text)])

        self.train_iter, self.valid_iter = data.BucketIterator.splits((train, valid),
                                                                      batch_size=batch_size,
                                                                      shuffle=shuffle,
                                                                      sort_key=lambda x: len(x.text),
                                                                      sort_within_batch=True)

        self.label.build_vocab(train)
        self.text.build_vocab(train, max_size=max_vocab, min_freq=min_freq)


def train_main():
    dataset = DataLoader("data/amazon_review.csv")

    vocab_size = len(dataset.text.vocab)
    n_classed = len(dataset.label.vocab)

    print(dataset.text)
    print(dataset.label)
    print(vocab_size)
    print(n_classed)

    """
    # Binary Classification
    model = RNNClassifier(input_size=vocab_size,
                         word_vec_dim=128,
                         hidden_size=64,
                         n_classes=2)

    crit = nn.NLLLoss()

    rnn_trainer = Trainer()
    rnn_model = rnn_trainer.train(model, crit, dataset)

    torch.save({'rnn': rnn_model,
                'vocab': dataset.text.vocab,
                'classes': dataset.label.vocab}, 'torch_rnn_model')
"""


def classify():
    saved_data = torch.load('torch_rnn_model')

    rnn_model = saved_data['rnn']
    vocab = saved_data['vocab']
    classes = saved_data['classes']

    vocab_size = len(vocab)

    with torch.no_grad():
        model = RNNClassifier(input_size=vocab_size,
                              word_vec_dim=128,
                              hidden_size=64)

train_main()
# classify()