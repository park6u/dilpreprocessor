import unittest
from preprocessing import buffer_read
from preprocessing import Regex
from preprocessing import Flag

class RegexTest(unittest.TestCase):
    def testRegex(self):
        ft = "<article>일본의 대표적인 우익 매체 산케이신문이 3일 중국 전승절 열병식에 １２３ａｂｃ？！ 참석하는 박근혜 대통령을 비판하며 일본 낭인에 의해 살해된 명성황후에 비유해 파장이 일고 있다.</article>"
        re = Regex(ft)
        re.filter_tag('article')
        re.filter_text(Flag.KOREAN.value)
        print(re.get_fulltext())

if __name__ == '__main__':
    unittest.main()
