import unittest
from nltk import sent_tokenize
from preprocessing.buffer_read import BufferRead
from preprocessing.encoding import Encoding
from preprocessing.regex import Regex
from preprocessing.regex import Flag
from preprocessing.token import Sentences
from preprocessing.token import Tokens
import os
import pandas as pd


class Proc_test(unittest.TestCase):

    def test_main(self):
        from pprint import pprint
        num_topics = [5, 6, 7, 10, 15, 20, 25, 30]
        for num_topics in num_topics:
            f = open('../output/topic_output/topic_count' + str(num_topics) + '.txt', 'r', encoding='utf-8')
            ll = eval(f.read())
            print('topic count : ' + str(num_topics))
            for i, v in enumerate(ll):
                print('topic count is ' + str(num_topics) + ' and topic ' + str(i))
                pprint(v)
            print('------------------------------------------------')
            f.close()

    def test_load_corpus(self):
        from gensim.models.ldamodel import LdaState
        from gensim.models import LdaModel
        from gensim.test.utils import datapath
        from pprint import pprint

        num_topic_list = [5,6,7,10]

        for num_topics in num_topic_list:
            temp = datapath('lda_model_topics_num_topic_' + str(num_topics))
            model = LdaModel.load(temp)

            pprint(model.top_topics())

    def test_make_copus(self):
        from gensim.models import LdaModel
        from gensim.corpora import Dictionary
        from gensim.test.utils import datapath
        #self.test_token()

        folders = os.listdir('../output/tokens/topic2/')
        total_file_list = []

        for folder in folders:
            files = os.listdir('../output/tokens/topic2/' + folder)
            for file in files:
                total_file_list.append('../output/tokens/topic2/' + folder + '/' + file)
        doc = [[] for i in range(len(total_file_list))]

        for i, file in enumerate(total_file_list):
            f = open(file, encoding='utf-8')
            doc[i] = eval(str(f.readline()))
            f.close()
        dictionary = Dictionary(doc)
        dictionary.filter_extremes(no_below=10, no_above=0.5)
        corpus = [dictionary.doc2bow(i) for i in doc]

        print('Number of unique tokens: %d' % len(dictionary))
        print('Number of documents: %d' % len(corpus))

        num_topic_list = [3, 4, 5]

        for num in num_topic_list:
            num_topics = num
            chunksize = 2000
            passes = 20
            iterations = 400
            eval_every = None

            temp = dictionary[0]
            id2word = dictionary.id2token

            model = LdaModel(corpus=corpus, id2word=id2word, chunksize=chunksize,
                             alpha='auto', eta='auto',
                             iterations=iterations, num_topics=num_topics,
                             passes=passes, eval_every=eval_every)

            top_topics = model.top_topics(corpus)

            avg_topic_coherence = sum([t[1] for t in top_topics]) / num_topics
            print('Average topic coherence: %.4f.' % avg_topic_coherence)

            from pprint import pprint
            pprint(top_topics)
            f = open('../output/topic_output/topic2_count' + str(num_topics) + '.txt', 'w', encoding='utf-8')
            f.write(str(top_topics))
            f.close()
            temp = datapath('lda_model_topic2_num_topic_' + str(num_topics))
            model.save(temp)

    def test_token(self):
        import xml.etree.ElementTree as elemTree
        from nltk.tokenize import RegexpTokenizer
        folders = os.listdir('../temp/topic2/')

        file_stopwords = open('../temp/news_stopwords.txt', 'r', encoding='utf-8-sig')
        stopwords = file_stopwords.readlines()
        stopwords = list(map(lambda x: x.strip(), stopwords))
        file_stopwords.close()

        def is_stopwords(word):
            for i in stopwords:
                if i == word:
                    return True
            return False

        for folder in folders:

            files = os.listdir('../temp/topic2/' + folder)
            os.mkdir('../output/tokens/topic2/' + folder)

            for file in files:
                doc = elemTree.parse('../temp/topic2/' + folder + '/' + file)
                article = doc.find('article').text
                if article is None:
                    continue
                regex = Regex(article)
                regex.set_stopwords(stopwords)
                regex.leave_text(Flag.KOREAN.value)
                article = regex.get_fulltext()

                tokenizer = RegexpTokenizer('\w+')
                tokens = tokenizer.tokenize(article)
                # tokens_copy = []
                # for token in tokens:
                #     if len(token) > 1 and not is_stopwords(token):
                #         tokens_copy.append(token)

                f = open('../output/tokens/topic2/' + folder + '/' + file + '.tokens', 'w', encoding='utf-8')
                f.write(str(tokens))
                f.close()

    def test_regex(self):
        file_stopwords = open('../temp/news_stopwords.txt', 'r', encoding='utf-8-sig')
        stopwords = file_stopwords.readlines()
        file_stopwords.close()

        regex = Regex('모바일한경은 PC·폰·태블릿에서 읽을 수 있는 프리미엄 뉴스 서비스입니다. [모바일한경 기사 더보기] [모바일한경 구독신청]ⓒ 한국경제신문, 무단 전재 및 재배포 금지')
        regex.set_stopwords(stopwords)
        regex.filter_stopwords()
        pass

    def test_word2vec(self):
        from embedding import word2vec
        m = word2vec.SkipGram()
        m.set_keyword()

    def test_browser(self):
        from selenium import webdriver
        browser = webdriver.Chrome('../tools/chromedriver')
        browser.get('https://news.naver.com/main/read.nhn?mode=LSD&mid=sec&sid1=102&oid=087&aid=0000240354')
        print(browser.execute_script("return $('#articleBodyContents')[0].outerText"))
        browser.quit()



    # todo 2019-09-19 crawling with multiprocessing
    def test_crawling(self):
        # import requests
        from time import sleep
        from random import randrange
        from selenium import webdriver
        from xml.etree.ElementTree import Element, dump
        from xml.etree.ElementTree import ElementTree
        from selenium.webdriver.chrome.options import Options
        from time import time
        import threading

        import re
        # from selenium.webdriver.common.alert import Alert
        # from multiprocessing import Process

        chrome_option = Options()

        chrome_option.add_experimental_option('prefs',
                                              {
                                                  "plugins.plugins_list": [
                                                      {"enabled": False, "name": "Chrome PDF Viewer"}],
                                                  "download":
                                                      {
                                                          "prompt_for_download": False,
#                                                          "default_directory": __temp__
                                                      }

                                              }
                                              )
        browser = webdriver.Chrome('../tools/chromedriver', options=chrome_option)

        def indent(elem, level=0):
            i = "\n" + level * "  "
            if len(elem):
                if not elem.text or not elem.text.strip():
                    elem.text = i + "  "
                if not elem.tail or not elem.tail.strip():
                    elem.tail = i
                for elem in elem:
                    indent(elem, level + 1)
                if not elem.tail or not elem.tail.strip():
                    elem.tail = i
            else:
                if level and (not elem.tail or not elem.tail.strip()):
                    elem.tail = i

        files = os.listdir('../data/')

        for file_name in files:
            f = open('../data/' + file_name, 'r', encoding='euc-kr')

            urls = f.readlines()
            f.close()
            # meta-data convention eg
            # 1 topic(input word): 헬스케어
            # 2 number of total news: 2432
            # 3 number of news: 957
            # 4 start_date: 2018.01.01
            # 5 end_date: 2018.01.1
            meta_topic = file_name
            # urls[0].split(':')[1].strip()
            meta_start_data = '0' #urls[3].split(':')[1].strip()
            meta_end_date = '1' #urls[4].split(':')[1].strip()
            folder_name = '{0}_{1}~{2}'.format(meta_topic, meta_start_data, meta_end_date)
            os.mkdir('../output/text/' + folder_name)

            # strip meta data
            urls = urls[5:len(urls)-1]

            digit = len(str(len(urls)))
            start = time()
            for index, url in enumerate(urls):
                offset = '0' * (digit - len(str(index)))
                _id = offset + str(index + 1)
                browser.get(url)
                root_node = Element('xml')
                title_node = Element('title')
                date_node = Element('date')
                article_node = Element('article')

                if url.find('sports') == -1:
                    # title
                    try:
                        title = browser.find_element_by_id('articleTitle')
                        title_node.text = title.text
                        root_node.append(title_node)
                    except Exception:
                        # Entertainment
                        continue

                    # date
                    try:
                        date = browser.find_element_by_class_name('t11').text
                        date_node.text = date
                        root_node.append(date_node)
                    except Exception:
                        # Entertainment
                        continue

                    # article
                    try:
                        article_text = browser.execute_script("return $('#articleBodyContents')[0].outerText")
                        article_node.text = article_text
                    except Exception:
                        # Entertainment
                        continue

                root_node.append(article_node)
                sleep(randrange(1, 3) * 0.1)
                indent(root_node)
                # write to xml
                ElementTree(root_node).write('../output/text/' + folder_name + '/' + _id + '.xml', encoding='utf-8')
            end = time()
            print(end - start)
        browser.quit()


if __name__ == '__main__':
    unittest.main()
