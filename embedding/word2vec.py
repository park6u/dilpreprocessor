import fasttext


class SkipGram:

    def __init__(self):
        pass

    def set_keyword(self):
        f = open('../data/nn', 'r', encoding='utf-8')
        s = ''
        for i in f.readlines():
            s += i
        f.close()
        model = fasttext.train_unsupervised('../data/nn', model='skipgram')
        print(model.words)
        model.save_model('../data/nn.bin')
        model = fasttext.load_model('../data/nn.bin')

