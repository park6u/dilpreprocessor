from collections import Counter
import urllib
import random
import webbrowser
import os
import sqlite3
from selenium import webdriver
import operator
import re
import urllib.request
import sci
import time
from bs4 import BeautifulSoup
from collections import OrderedDict
import matplotlib.pyplot as plt
from konlpy.tag import Hannanum

from lxml import html
import sys

if sys.version_info[0] >= 3:
    urlopen = urllib.request.urlopen
else:
    urlopen = urllib.urlopen

r = lambda: random.randint(0,255)
color = lambda: (r(), r(), r())

def get_database():
    data_path = os.path.expanduser('~') + "\Desktop"
    files = os.listdir(data_path)
    history_db = os.path.join(data_path, 'history.db')

    c = sqlite3.connect(history_db)
    cursor = c.cursor()
    select_statement = "SELECT DISTINCT urls.url, urls.visit_count FROM urls, visits;"
    cursor.execute(select_statement)

    results = cursor.fetchall()

    return results


def get_url_fulltext(res):
    wordtoken = []
    for url, count in res:
        pattern = re.compile('(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)')
        if len(pattern.findall(url)) > 0 or url.find('code') != -1: continue
        wordtoken += request_url(url)
    return wordtoken

def request_url(url):
    browser.get(url)
    fullhtml = browser.page_source
    pattern = re.compile('[가-힣]+')
    time.sleep(0.5)
    return pattern.findall(fullhtml)


if __name__ == '__main__':
    browser = webdriver.Chrome('C:\\Users\pkjoh\Downloads\chromedriver')
    res = get_database()
    res = get_url_fulltext(res)
    print(len(res))
    print(res)


