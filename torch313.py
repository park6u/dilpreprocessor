import random
from torch import nn

class MyLinear(nn.Module):

    def __init__(self, input_size, output_size):
        super(MyLinear, self).__init__()
        self.linear = nn.Linear(input_size, output_size)

    def forward(self, *input):
        return self.linear(input)

def train(self,model, x, y, optim):
    """
        Keyword Arguments
        model -- nn to train
        x -- input of nn, size of input node equal to dim(x)
        y -- expecting result
        optim -- optimizer

        y_hat -- approximation of nn result
        loss -- loss of result, using MSE function, (y - y_hat) ** 2 / dim(x), scalar value

        return data of loss
    """
    optim.zero_grad()

    y_hat = model(x)
    loss = ((y-y_hat)**2).sum()

    # do backprop algorithm
    loss.backward()

    # do gradient descent with one step
    optim.step()

    return loss.data

